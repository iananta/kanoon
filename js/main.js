this.initial=function(){
	let _this=this
	this.mobileMenuEventListener=function(){
		let selector=$('.bar-btn')
		selector.on('click',function(){
			let parent=$(this).parent()
			let menu=parent.next()
				menu.toggleClass('active')
			if(menu.hasClass('active')){
				menu.slideDown()
			}else{
				menu.slideUp()
			}
		})
	}
	this.dropDownEventListener=function(){
		let selector=$('.kanoon-button')
		selector.on('click',function(e){
			let next=$(this).next()
				next.toggleClass('active')
				if(next.hasClass('active')){
					$(this).find('.kanoon-icon').html('&minus;')
					next.slideDown();
				}else{
					$(this).find('.kanoon-icon').html('&plus;')
					next.slideUp()
				}
		})
	}
	this.popupListener=function(){
		let popBtn=$('.popup-btn')
		let dismiss=$('.popup-dismiss')
			popBtn.on('click',function(){
				let target=$(this)
				let targetBlock='#'+target.data('target')
				$(targetBlock).fadeIn()
				$(targetBlock).find('.popup-wrapper').css('transform','translate(0%)')
				$('html').css('overflow','hidden')
			})

			dismiss.on('click',function(){
				$('.popup-block').fadeOut()
				$('.popup-wrapper').removeAttr('style')
				$('html').removeAttr('style')

			})

	}
	this.init=function(){
		_this.mobileMenuEventListener()
		_this.dropDownEventListener()
		_this.popupListener()
	}
}
let obj=new initial()
	obj.init()